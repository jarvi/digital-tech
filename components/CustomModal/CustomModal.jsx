import { useEffect, useState } from "react";
import useData from "../../hooks/useData";
import CreatePost from "../CreatePost/CreatePost";

export default function CustomModal() {
  const { modalVisibility, setModalVisibility } = useData();

  function handleFather() {
    setModalVisibility(false);
  }

  function handleChild(e) {
    if(e.target.classList[0] === "close") {
      setModalVisibility(false);
    } else {
      e.stopPropagation();
    }
  }

  return modalVisibility && (
    <div onClick={handleFather} className="custom-modal">
      <div onClick={handleChild} className="dialog">
        <div className="modal-header">
          <span className="close"></span>
        </div>
        <div className="modal-content">
          <span className="modal-title">Cree su Post</span>
          <CreatePost />
          <div className="group" id="text-modal"></div>
        </div>
        <div className="modal-action"></div>
      </div>
    </div>
  );
}
