import { useEffect } from "react";
//import io from "socket.io-client";
import useData from "./useData";
import { useLocalStorage } from "./useLocalStorage";
import useHttpPut from "../hooks/useHttpPut";
import { APIS, END_POINTS } from "../util/constants";

let socket;
export default async function useSocketInitializer(setIsThereNewPost = "") {
  const { setSocket, setPosts, posts, setReloadUser, socket } = useData();
  //const localStorageKey = "user-logged";
  //const [collection] = useLocalStorage(localStorageKey);

  //console.log(socketInit);
  // const { response, error, isLoading } = useHttpPut(
  //   `${APIS.MOCKAPI_POSTS}${END_POINTS.PUT}${idPost}`,
  //   dateLikePrepared,
  //   RunUpdate
  // );

  useEffect(() => {
    console.log(socket);
    if (socket) {
      const SocketInitializerConection = async () => {
        // await fetch("/api/socket");

        // socket = io();

        // setSocket(socket);
        // console.log("socket iniciado");

        socket.on("receive-message", (data) => {
          //console.log("escuchando el evento receive-message");
          ////console.log(data);
          //alert(JSON.stringify(data));
          setIsThereNewPost(data);

          //setAllMessages((pre) => [...pre, data]);
        });

        socket.on("receive-likes", (data) => {
          console.log("%cEscuchando el evento receive-likes", "color:green");

          const idPost = data.id;

          setPosts((p) => {
            //console.log("todos los posts array:", p);

            const index = p.findIndex((obj) => obj.id === idPost);
            //  console.log("indice del array posts", index);
            //  console.log(p);

            p[index] = data;
            // console.log(p);
            return p;
          });
          setReloadUser((x) => !x);
        });
      };
      SocketInitializerConection();
    }
  }, [socket]);
}
