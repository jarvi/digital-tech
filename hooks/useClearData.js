import { useState, useEffect, useCallback } from "react";
import { useLocalStorage } from "../hooks/useLocalStorage";
import useData from "../hooks/useData";

export default function useClearData() {
  const { removeLocalStorage: removeUserLoggedStorage } =
    useLocalStorage("user-logged");

  const { removeLocalStorage: removeNumberPublicationsStorage } =
    useLocalStorage("number-publications");

  const { setNumberPublications: setNumberPublicationsContext } = useData();
  
  
  useEffect(() => {
    removeUserLoggedStorage("user-logged");
    removeNumberPublicationsStorage("number-publications");

    setNumberPublicationsContext(0);
  }, []);
}
