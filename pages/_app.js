import "../styles/globals.scss";

import { useMemo, useState, useEffect } from "react";
import { GlobalData } from "../context/GlobalData";

//import useSocketConexion from "../hooks/useSocketConexion";

export default function App({ Component, pageProps }) {
  //const { sc } = useSocketConexion();
  ////console.log('%cToda la app se ha re-renderizado','font-size:13px;color:red ' );

  const [posts, setPosts] = useState([]);
  const [formLoginData, setFormLoginData] = useState("");
  const [formRegisterData, setFormRegisterData] = useState("");
  const [formPostData, setformPostData] = useState("");
  const [modalVisibility, setModalVisibility] = useState(false);
  const [reloadUser, setReloadUser] = useState(false);
  const [socket, setSocket] = useState(null);
  const [userData, setUserData] = useState(null);
  const [numberPublications, setNumberPublications] = useState(null);

  const stateGlobal = useMemo(
    (_) => ({

      numberPublications,
      setNumberPublications,

      userData,
      setUserData,

      posts,
      setPosts,

      formLoginData,
      setFormLoginData,

      formRegisterData,
      setFormRegisterData,

      formPostData,
      setformPostData,

      modalVisibility,
      setModalVisibility,

      reloadUser,
      setReloadUser,

      socket,
      setSocket,
      
    }),
    [
      numberPublications,
      userData,
      posts,
      formLoginData,
      formRegisterData,
      formPostData,
      modalVisibility,
      reloadUser,
      socket,
    ]
  );

  return (
    <GlobalData.Provider value={stateGlobal}>
      <Component {...pageProps} />
    </GlobalData.Provider>
  );
}
