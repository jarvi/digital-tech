/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useRef, useCallback, memo } from "react";
import Post from "../components/Post/Post";
import SearchBar from "../components/SearchBar/SearchBar";
import Perfil from "../components/Perfil/Perfil";
import CustomModal from "../components/CustomModal/CustomModal";
import useData from "../hooks/useData";
import useUpdatePosts from "../hooks/useUpdatePosts";
import useSocketInitializer from "../hooks/useSocketInitializer";
import useCalculateWindow from "../hooks/useCalculateWindow";
import useSocketConexion from "../hooks/useSocketConexion";
import { useRouter } from "next/router";
import { useLocalStorage } from "../hooks/useLocalStorage";

function dashboard() {
  //console.log("dashboard se ha renderizado");
  const router = useRouter();
  //const { userData } = useData();
  const { objCollection: dataUserLoggedStorage } =
    useLocalStorage("user-logged");

  useSocketConexion();
  const inputRef = useRef(null);
  const [isThereNewPost, setIsThereNewPost] = useState(false);
  useSocketInitializer(setIsThereNewPost);
  useCalculateWindow(inputRef);

  const [customParams, setCustomParams] = useState({
    completed: false,
    page: 1,
    limit: 2,
    sortBy: "createdAt",
    order: "desc",
  });

  const { isLoading } = useUpdatePosts(customParams);

  const {
    posts: localPosts,
    setPosts,
    modalVisibility,
    setModalVisibility,
    socket,
  } = useData();

  const getMorePosts = () => {
    ////console.log("getMorePosts");
    setCustomParams((currentParams) => {
      return { ...currentParams, page: currentParams.page + 1 };
    });
  };

  useEffect(() => {
    return () => {
      //console.log('%cCerrando la conexion con socket', 'border:solid 1px red');
      socket?.disconnect();
      //console.log(socket);
    };
  }, []);

  useEffect(() => {
    //console.log(dataUserLoggedStorage);
    if (!dataUserLoggedStorage) {
      router.push("/");
    }
  }, []);

  return (
    socket && dataUserLoggedStorage && (
      <div className="dashboardPage">
        {modalVisibility && <CustomModal />}
        {/* <SearchBar /> */}
        <div className="containerDash">
          <div
            onClick={() => {
              setModalVisibility((x) => !x);
            }}
            className="form-create-post"
          >
            Crear publicacion
          </div>
          <Perfil />

          <div className="content-posts">
            {isThereNewPost && (
              <button
                onClick={() => {
                  setPosts((prevPosts) => {
                    return [isThereNewPost, ...prevPosts];
                  });

                  setIsThereNewPost(false);
                }}
                className="button magin-auto"
              >
                {"Nuevas publicaciones disponibles"}
              </button>
            )}
            {localPosts &&
              localPosts.map((post, key) => <Post dataPost={post} key={key} />)}
            <div ref={inputRef} onClick={getMorePosts}></div>
          </div>
        </div>
      </div>
    )
  );
}

export default dashboard;
//export default memo(dashboard)
